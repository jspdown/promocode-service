'use strict';

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PromoCodeSchema = new Schema({
  name: { type: String, unique: true },
  avantage: {
    percent: Number
  },
  restrictions: Object
});

const PromoCode = mongoose.model('PromoCode', PromoCodeSchema);

module.exports = PromoCode;
