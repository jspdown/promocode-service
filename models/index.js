'use strict';

const bluebird = require('bluebird');
const mongoose = require('mongoose');

// Mongoose will use bluebird promises
mongoose.Promise = bluebird;
mongoose.set('useCreateIndex', true);

const PromoCode = require('./promo-code');

const DB_ERRORS = {
  DUPLICATE_KEY: 11000
};

module.exports = { PromoCode, DB_ERRORS };
