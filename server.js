'use strict';

const http = require('http');
const express = require('express');
const helmet = require('helmet');
const Promise = require('bluebird');
const bodyParser = require('body-parser');
const database = require('./libs/database');
const redis = require('./libs/redis');
const errorHandler = require('./middlewares/error-handler');
const { responseTime, startTime } = require('./middlewares/response-time');
const config = require('./config');
const router = require('./router');

Promise.all([
  database.connect(config.mongodb),
  redis.connect(config.redis)
])
  .then(() => {
    const app = express();
    const server = http.Server(app);

    app
      .disable('x-powered-by')
      .use(helmet())
      .use(bodyParser.json())
      .use(startTime)
      .use(router)
      .use(errorHandler)
      .use(responseTime);

    server.listen(config.port, () =>
      console.info('[server] start listening', { port: config.port })
    );
  });
