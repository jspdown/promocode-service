'use strict';

const { PromoCode, DB_ERRORS } = require('../models');
const { BaseError } = require('../libs/error');

class NotFoundError extends BaseError {
  constructor() { super("Promocode not found"); }
}

class AlreadyExistsError extends BaseError {
  constructor() { super("Promocode already exists"); }
}

/**
 * find search for the given promocode name in the
 * database.
 *
 * @throw NotFoundError
 *
 * @param  {String} name Promocode name
 * @return {PromoCode}      Resolved with the promocode found
 */
function find(name) {
  return PromoCode.findOne({ name })
    .then(promocode => {
      if (!promocode) {
        throw new NotFoundError();
      }

      return {
        name: promocode.name,
        avantage: promocode.avantage,
        restrictions: promocode.restrictions
      };
    });
}

/**
 * create persists a new promocode in the database
 *
 * @throw AlreadyExistsError
 *
 * @param  {String} name         Name of the promocode
 * @param  {Object} avantage     Avantage given by the promocode
 * @param  {Array}  restrictions List of restrictions
 * @return {Promise}             Resolved once created
 */
function create(name, avantage, restrictions) {
  const promoCode = new PromoCode({
    name, avantage, restrictions
  });

  return promoCode.save()
    .catch(err => {
      if (err && err.code == DB_ERRORS.DUPLICATE_KEY) {
        throw new AlreadyExistsError();
      }

      throw err;
    });
}

module.exports = {
  find, create,
  AlreadyExistsError,
  NotFoundError
};
