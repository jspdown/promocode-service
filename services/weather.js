'use strict';

const _ = require('lodash');
const request = require('request');
const Promise = require('bluebird');
const config = require('../config');
const redis = require('../libs/redis');
const { BaseError } = require('../libs/error');

class NotFoundError extends BaseError {
  constructor() { super('Weather not found'); }
}

const WEATHER_TYPES = {
  CLEAR: 'clear',
  RAIN: 'rain',
  UNKNOWN: 'unknown'
};

/**
 * find returns the weather in the given town
 *
 * Returns:
 * {
 *   "type": <WEATHER_TYPE>
 *   "temperature": <TEMPERATURE>
 * }
 *
 * @param  {String} town Town name
 * @return {Promise}     Resolved with the weather
 */
function find(town) {
  return getWeather(town)
    .then(weather => ({
      type: OpenWeatherMainToType(_.get(weather, 'weather.0.main')),
      temperature: _.get(weather, 'main.temp'),
    }));
}

const getWeather = _.wrap(callOpenWeatherMapAPI, (original, town) => {
  return redis.get(town)
    .catch(err => {
      console.error('cannot get cached weather', err);

      return null;
    })
    .then(weather => {
      if (weather) return weather;

      return original(town)
        .then(weather => redis.store(town, weather, 3600)
          .then(() => weather)
          .catch(err => {
            console.error('cannot cache weather', err);

            return weather
          }));
    });
});

/**
 * OpenWeatherMainToType converts OpenWeatherMap 'main'
 * into a WEATHER_TYPES.
 *
 * @param  {String} main Main weather
 * @return {String} Weather type
 */
function OpenWeatherMainToType(main) {
  if (main === 'Rain') {
    return WEATHER_TYPES.RAIN;
  } else if (main === 'Clear') {
    return WEATHER_TYPES.CLEAR;
  } else {
    return WEATHER_TYPES.UNKNOWN;
  }
}

/**
 * callOpenWeatherMapAPI makes an HTTP call to the
 * OpenWeatherMap API to get the weather in the given city
 *
 * @param  {String} city City
 * @return {Object}      Resolved with the forecast
 */
function callOpenWeatherMapAPI(city) {
  const endpoint = 'http://api.openweathermap.org/data/2.5/weather';
  const options = {
    json: true,
    qs: {
      APPID: config.openWeatherMapAppID,
      q: city,
      units: 'metric'
    }
  };

  return new Promise((resolve, reject) => {
    request(endpoint, options, (err, res, body) => {
      if (err) {
        return reject(err);
      } else if (body && body.cod === '404') {
        return reject(new NotFoundError());
      } else if (body && body.cod !== 200) {
        return reject(new Error(body.message));
      }

      resolve(body);
    });
  });
}

module.exports = {
  find,
  NotFoundError,
  WEATHER_TYPES
};
