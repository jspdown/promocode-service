const _ = require('lodash');
const moment = require('moment');
const { WEATHER_TYPES } = require('./weather');

const OPERATORS = {
  EQ: '@eq',
  LT: '@lt',
  GT: '@gt',
  AND: '@and',
  OR: '@or'
};

const FIELDS = {
  WEATHER: 'weather',
  TEMPERATURE: 'temperature',
  AGE: 'age',
  DATE: 'date'
};

const WEATHER_VALUES = {
  CLEAR: WEATHER_TYPES.CLEAR,
  RAIN: WEATHER_TYPES.RAIN
};

/**
 * isValidBooleanOperator checks if the given list of restrictions
 * is valid in the case of a array based operator (or/and)
 *
 * @param  {String}  operator OPERATORS.AND or OPERATORS.OR
 * @return {String|null}      Returns an error string if invalid
 */
const isValidBooleanOperator = operator => restrictions => {
  if (!_.isArray(restrictions)) {
    return `"${operator}" operator must be an array`;
  } else if (!restrictions.length) {
    return `"${operator}" must have at least one condition`;
  }

  const errors = [];

  for (let restriction of restrictions) {
    const error = isValidRestriction(restriction);

    if (error) {
      errors.push(error);
    }
  }

  if (errors.length) {
    return `invalid "${operator}" operator: ${errors.join(', ')}`;
  }

  return null;
};

// Check boolean operator AND
const isValidAnd = isValidBooleanOperator(OPERATORS.AND);
// Check boolean operator OR
const isValidOr = isValidBooleanOperator(OPERATORS.OR);


/**
 * isValidRestriction check if a given restriction is valid.
 * A restriction must follow these rules:
 * - Must be an object
 * - Must have at least one condition
 * - Must contains one of these:
 *  - OPERATORS.AND
 *  - OPERATORS.OR
 *  - FIELDS.WEATHER
 *  - FIELDS.AGE
 *  - FIELDS.TEMPERATURE
 *  - FIELDS.DATE
 *
 * @param  {Object}  condition Condition to validate
 * @return {String|null}       Returns an error string if invalid
 */
function isValidRestriction(condition) {
  if (!_.isObject(condition)) {
    return 'condition must be an object';
  } else if (Object.keys(condition).length != 1) {
    return 'condition must contain exactly one field';
  }

  if (!_.isUndefined(condition[OPERATORS.AND])) {
    return isValidAnd(condition[OPERATORS.AND]);
  } else if (!_.isUndefined(condition[OPERATORS.OR])) {
    return isValidOr(condition[OPERATORS.OR]);
  } else if (!_.isUndefined(condition[FIELDS.WEATHER])) {
    return isValidMeteo(condition);
  } else if (!_.isUndefined(condition[FIELDS.AGE])) {
    return isValidAge(condition);
  } else if (!_.isUndefined(condition[FIELDS.TEMPERATURE])) {
    return isValidTemperature(condition);
  } else if (!_.isUndefined(condition[FIELDS.DATE])) {
    return isValidDate(condition);
  }

  return `unknown field "${Object.keys(condition)[0]}"`;
}

/**
 * isValidMeteo checks if a FIELDS.WEATHER is valid.
 * Rules are:
 * - Must be an object
 * - Must contains operator OPERATORS.EQ
 * - Value must be a string containing one of these:
 *    - WEATHER_VALUES.CLEAR
 *    - WEATHER_VALUES.RAIN
 *
 * @param  {Object}  condition Condition to validate
 * @return {String|null}       Returns an error string if invalid
 */
const isValidMeteo = cond => isValidCondition(cond, FIELDS.WEATHER, {
  [OPERATORS.EQ]: {
    condition: isValidMeteoValue,
    error: `must be either ${Object.values(WEATHER_VALUES).join(', ')}`
  }
});

/**
 * isValidAge checks if a FIELDS.AGE is valid.
 * Rules are:
 * - Must be an object
 * - Must contains one of these operators:
 *  - OPERATORS.EQ
 *  - OPERATORS.LT
 *  - OPERATORS.GT
 * - The value must be a number > 0
 *
 * @param  {Object}  condition Condition to validate
 * @return {String|null}       Returns an error string if invalid
 */
const ageError = 'must be a positive number';
const isValidAge = cond => isValidCondition(cond, FIELDS.AGE, {
  [OPERATORS.EQ]: { condition: isValidAgeValue, error: ageError },
  [OPERATORS.LT]: { condition: isValidAgeValue, error: ageError },
  [OPERATORS.GT]: { condition: isValidAgeValue, error: ageError }
});

/**
 * isValidTemperature checks if a FIELDS.TEMPERATURE is valid.
 * Rules are:
 * - Must be an object
 * - Must contains one of these operators:
 *  - OPERATORS.EQ
 *  - OPERATORS.LT
 *  - OPERATORS.GT
 * - The value must be a number
 *
 * @param  {Object}  condition Condition to validate
 * @return {String|null}       Returns an error string if invalid
 */
const temperatureError = 'must be a number';
const isValidTemperature = cond => isValidCondition(cond, FIELDS.TEMPERATURE, {
  [OPERATORS.EQ]: { condition: v => _.isNumber(v), error: temperatureError },
  [OPERATORS.LT]: { condition: v => _.isNumber(v), error: temperatureError },
  [OPERATORS.GT]: { condition: v => _.isNumber(v), error: temperatureError }
});

/**
 * isValidDate checks if a FIELDS.DATE is valid.
 * Rules are:
 * - Must be an object
 * - Must contains one of these operators:
 *  - OPERATORS.EQ
 *  - OPERATORS.LT
 *  - OPERATORS.GT
 * - The value must be a string matching "dd-mm-yyyy";
 *
 * @param  {Object}  condition Condition to validate
 * @return {String|null}       Returns an error string if invalid
 */
const dateError = 'must be a valid date "dd-mm-yyyy"';
const isValidDate = cond => isValidCondition(cond, FIELDS.DATE, {
  [OPERATORS.EQ]: { condition: isValidDateValue, error: dateError },
  [OPERATORS.LT]: { condition: isValidDateValue, error: dateError },
  [OPERATORS.GT]: { condition: isValidDateValue, error: dateError }
});

const isValidMeteoValue = value => {
  if (!_.isString(value)) return false;

  return Object.values(WEATHER_VALUES).includes(value);
};

const isValidAgeValue = value => {
  if (!_.isNumber(value)) return false;

  const age = parseInt(value, 10);

  return age >= 0;
};

const isValidDateValue = value => moment(value, 'DD-MM-YYYY', true).isValid();

/**
 * isValidCondition checks if the given condition is valid.
 * A condition must follow these rules:
 * - Must be an object
 * - Contains at least one operator listed in `operators`
 * - Mustn't contain unknown operators
 * - Operators OPERATORS.EQ cannot be used in the same time
 * as OPERATORS.LT and OPERATORS.GT
 *
 * @param  {Object}  restriction [description]
 * @param  {String}  field       [description]
 * @param  {Object}  operators   Hash map of operators
 * @return {String|null}         Returns an error string if invalid
 */
function isValidCondition(restriction, field, operators) {
  const isObject = !_.isObject(restriction[field]);
  const operatorNames = Object.keys(restriction[field]);
  const atLeastOneOperator = operatorNames.length > 0;

  // Make sure there is at least one operator in the restriction
  if (isObject) {
    return `"${field}" must be an object`;
  } else if (!atLeastOneOperator) {
    return `"${field}" must contain at least one operator`;
  }

  let errors = [];

  // Check if restrictions are valid for the given list of valid operators
  Object.keys(restriction[field])
    .forEach(operator => {
      if (_.isUndefined(operators[operator])) {
        errors.push(`"${field}" contains unknown operator "${operator}"`);
        return;
      }

      const { condition, error } = operators[operator];

      if (!condition(restriction[field][operator])) {
        errors.push(`"${field}.${operator}" ${error}`);
      }
    })

  // Make sure we can't use @eq with @lt and @gt
  if (!_.isUndefined(restriction[field][OPERATORS.EQ]) && (
    !_.isUndefined(restriction[field][OPERATORS.LT]) ||
    !_.isUndefined(restriction[field][OPERATORS.GT])
  )) {
    errors.push(`cannot use "${OPERATORS.EQ}" with "${OPERATORS.LT}" and "${OPERATORS.GT}"`);
  }

  if (errors.length) {
    return `invalid field "${field}": ${errors.join(', ')}`;
  }
  return null;
}

const REASONS = {
  UNKNOWN: 'Unknown',
  DATE: {
    NOT_EQUAL: 'DateNotEqual',
    NOT_LT: 'DateNotLessThan',
    NOT_GT: 'DateNotGreaterThan'
  },
  TEMPERATURE: {
    NOT_EQUAL: 'TemperatureNotEqual',
    NOT_LT: 'TemperatureNotLessThan',
    NOT_GT: 'TemperatureNotGreaterThan'
  },
  AGE: {
    NOT_EQUAL: 'AgeNotEqual',
    NOT_LT: 'AgeNotLessThan',
    NOT_GT: 'AgeNotGreaterThan'
  },
  WEATHER: {
    NOT_EQUAL: 'WeatherNotEqual'
  }
};

/**
 * checkOr checks if at least of the given restrictions
 * is valid.
 *
 * @param  {Object} user        User data
 * @param  {Array} restrictions List of restrictions
 * @return {String}             Error reason
 */
function checkOr(user, restrictions) {
  let reason = null;

  for (let restriction of restrictions) {
    const restrictionReason = checkRestriction(user, restriction);

    if (!restrictionReason) return null;
    reason = restrictionReason;
  }

  return reason;
}

/**
 * checkAnd checks if all the given restrictions are valid.
 *
 * @param  {Object} user        User data
 * @param  {Array} restrictions List of restrictions
 * @return {String}             Error reason
 */
function checkAnd(user, restrictions) {
  for (let restriction of restrictions) {
    const reason = checkRestriction(user, restriction);

    if (reason) return reason;
  }

  return null;
}

/**
 * checkRestriction checks if the given restriction is valid.
 *
 * @param  {Object} user        User data
 * @param  {Array} restrictions List of restrictions
 * @return {String}             Error reason
 */
function checkRestriction(user, restriction) {
  if (!_.isUndefined(restriction[OPERATORS.AND])) {
    return checkAnd(user, restriction[OPERATORS.AND]);
  } else if (!_.isUndefined(restriction[OPERATORS.OR])) {
    return checkOr(user, restriction[OPERATORS.OR]);
  } else if (!_.isUndefined(restriction[FIELDS.WEATHER])) {
    return checkWeather(user, restriction[FIELDS.WEATHER]);
  } else if (!_.isUndefined(restriction[FIELDS.AGE])) {
    return checkAge(user, restriction[FIELDS.AGE]);
  } else if (!_.isUndefined(restriction[FIELDS.TEMPERATURE])) {
    return checkTemperature(user, restriction[FIELDS.TEMPERATURE]);
  } else if (!_.isUndefined(restriction[FIELDS.DATE])) {
    return checkDate(user, restriction[FIELDS.DATE]);
  }

  return REASONS.UNKNOWN;
}

function checkWeather(user, weather) {
  return user.weather !== weather[OPERATORS.EQ]
    ? REASONS.WEATHER.NOT_EQUAL
    : null;
}

function checkAge(user, age) {
  if (!_.isUndefined(age[OPERATORS.EQ])) {
    return user.age !== age[OPERATORS.EQ]
      ? REASONS.AGE.NOT_EQUAL
      : null;
  }

  if (!_.isUndefined(age[OPERATORS.LT])
    && user.age >= age[OPERATORS.LT]) {

    return REASONS.AGE.NOT_LT;
  } else if (!_.isUndefined(age[OPERATORS.GT])
    && user.age <= age[OPERATORS.GT]) {

    return REASONS.AGE.NOT_GT;
  }
  return null;
}

function checkTemperature(user, temperature) {
  if (!_.isUndefined(temperature[OPERATORS.EQ])) {
    return user.temperature !== temperature[OPERATORS.EQ]
      ? REASONS.TEMPERATURE.NOT_EQUAL
      : null;
  }

  if (!_.isUndefined(temperature[OPERATORS.LT])
    && user.temperature >= temperature[OPERATORS.LT]) {

    return REASONS.TEMPERATURE.NOT_LT;
  } else if (!_.isUndefined(temperature[OPERATORS.GT])
    && user.temperature <= temperature[OPERATORS.GT]) {

    return REASONS.TEMPERATURE.NOT_GT;
  }
  return null;
}

function checkDate(user, date) {
  if (!_.isUndefined(date[OPERATORS.EQ])) {
    return user.date !== date[OPERATORS.EQ]
      ? REASONS.DATE.NOT_EQUAL
      : null;
  }

  if (!_.isUndefined(date[OPERATORS.LT])
    && user.date >= moment(date[OPERATORS.LT])) {

    return REASONS.DATE.NOT_LT;
  } else if (!_.isUndefined(date[OPERATORS.GT])
    && user.date <= moment(date[OPERATORS.GT])) {

    return REASONS.DATE.NOT_GT;
  }
  return null;
}

module.exports = {
  validate: isValidAnd,
  check: checkAnd
};
