'use strict';

const weather = require('./weather');
const restrictions = require('./restrictions');
const promocode = require('./promocode');

module.exports = { weather, restrictions, promocode };
