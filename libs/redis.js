'use stict'

const redis = require('redis');

let client = null;

function connect(uri) {
  client = redis.createClient(uri);

  return new Promise((resolve, reject) => {
    client.on('connect', () => {
      console.log('[redis] connected');
      resolve();
    });
    client.on('error', reject);
  });
}

function store(key, data, ttl) {
  return new Promise((resolve, reject) => {
    if (client === null) {
      return reject(new Error('not connected to redis'));
    }

    client.set(key, JSON.stringify(data), 'EX', ttl, err => {
      if (err) return reject(err);
      resolve();
    });
  });
}

function get(key) {
  return new Promise((resolve, reject) => {
    if (client === null) {
      return reject(new Error('not connected to redis'));
    }

    return client.get(key, (err, reply) => {
      if (err) return reject(err);

      try {
        const data = JSON.parse(reply);

        return resolve(data);
      } catch (err) {
        return resolve(null);
      }
    });
  });
}

module.exports = { connect, store, get };
