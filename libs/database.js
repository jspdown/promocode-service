'use strict';

const Promise = require('bluebird');
const mongoose = require('mongoose');

function connect(connString) {
  mongoose.connect(connString, { useNewUrlParser: true });

  return new Promise((resolve, reject) => {
    mongoose.connection.once('connected', resolve);
    mongoose.connection.on('error', reject);
  })
    .then(() => console.info('[mongodb] connected'));
}

module.exports = { connect };
