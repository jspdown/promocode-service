'use strict';

class ErrorCode {
  constructor(status, id, message) {
    this.status = status;
    this.id = id;
    this.message = message;
  }

  toJSON() {
    return {
      status: this.status,
      id: this.id,
      message: this.message
    };
  }
}

const code = (id, message, status) => new ErrorCode(status || 400, id, message);

const HTTP = {
  BAD_REQUEST:      code(0, 'Bad request', 400),
  UNAUTHORIZED:     code(1, 'Unauthorized', 401),
  FORBIDDEN:        code(2, 'Forbidden', 403),
  NOT_FOUND:        code(3, 'Not found', 404),
  INTERNAL_ERROR:   code(4, 'Internal error', 500),
  NOT_IMPLEMENTED:  code(5, 'Not Implemented', 501)
};

const COMMON = {
  ALREADY_EXISTS:       code(8, 'Already exists', 400),
  SERVICE_UNAVAILABLE:  code(9, 'Service Unavailable', 503),
  MALFORMED_JSON:       code(10, 'Malformed JSON'),
  RATE_LIMIT_EXCEEDED:  code(11, 'Rate limit exceeded')
};

class BaseError extends Error {
  constructor(message) {
    super(message);

    if (typeof (Error.captureStackTrace) === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = (new Error(message)).stack;
    }
  }
}

class ApiError extends BaseError {
  constructor(errorCode = HTTP.INTERNAL_ERROR, data = {}) {
    super(errorCode.message);

    this.name = this.constructor.name;
    this.data = data;
    this.message = errorCode.message;
    this.code = errorCode.id;
    this.status = errorCode.status;
  }

  toJSON() {
    return {
      code: this.code,
      status: this.status,
      message: this.message,
      data: this.data
    };
  }
}

module.exports = {
  BaseError, ApiError, ErrorCode,
  codes: { HTTP, COMMON }
};
