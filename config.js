'use strict';

const _ = require('lodash');

function getInt(varName, defaultValue) {
  const strValue = process.env[varName];

  if (!_.isUndefined(strValue)) {
    const value = parseInt(strValue);

    if (!_.isNaN(value)) {
      return value;
    }
  }

  return defaultValue;
}

function getStr(varName, defaultValue) {
  const value = process.env[varName];

  if (_.isUndefined(value)) {
    if (_.isUndefined(defaultValue)) {
      throw new Error(`env variable ${varName} is not defined`);
    }

    return defaultValue;
  }
  return value;
}


const config = {
  env: getStr('NODE_ENV', 'dev'),
  port: getInt('PORT', 9000),
  mongodb: getStr('MONGODB_URI', 'mongodb://localhost:27017/promo'),
  redis: getStr('REDIS_URL', 'redis://localhost:6379'),
  openWeatherMapAppID: getStr('OPEN_WEATHER_MAP_APP_ID')
};

module.exports = config;
