'use strict';

const Prometheus = require('prom-client');

const metric = new Prometheus.Histogram({
  name: 'http_request_duration_ms',
  help: 'Duration of HTTP requests in ms',
  labelNames: ['method', 'path'],
  buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500]
});

const startTime = (req, res, next) => {
  res.locals.startEpoch = Date.now()
  next();
};

const responseTime = (req, res, next) => {
  const responseTimeInMs = Date.now() - res.locals.startEpoch;

  metric
    .labels(req.method, req.path)
    .observe(responseTimeInMs);

  next();
};

module.exports = { startTime, responseTime };
