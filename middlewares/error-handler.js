'use strict';

const { ApiError, codes } = require('../libs/error');

function errorHandler(err, req, res, next) {
  let error = err;

  if (error instanceof SyntaxError && error.status === 400 && 'body' in error) {
    error = new ApiError(codes.COMMON.MALFORMED_JSON);
  } else if (!(error instanceof ApiError)) {
    console.error(codes.HTTP.INTERNAL_ERROR, {
      name: error.name,
      message: error.message,
      stack: error.stack
    });

    error = new ApiError(codes.HTTP.INTERNAL_ERROR);
  }

  res.status(error.status || 400).send({ error: error.toJSON() });

  next();
}

module.exports = errorHandler;
