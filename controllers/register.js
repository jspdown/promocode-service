'use strict';

const _ = require('lodash');
const services = require('../services');
const { ApiError, codes } = require('../libs/error');

/**
 * Validate is a middleware responsible for validating the incoming request.
 *
 * Rules are:
 * - "name" if required and must be a string
 * - "avantage" is required and must be an object
 * - "avantage.percent" is required and must be a number between 0 and 100
 * - "restrictions" is optional.
 *
 * "restrictions" is an array of restriction rules. Rules can be of the follow types:
 * - value rule
 * - boolean rule
 *
 * A value rule is defined as: "{ <name>: { <operator>: <value } }"
 * Where:
 * - <name>: field name, one of "weather", "age", "temperature" or "date"
 * - <operator>: operator name, one of "@eq", "@lt", "@gt". Some field names
 * don't allow all operators: "weather" can only use "@eq"
 * - <value>: comparison value: Depending on the field name it must have the following type:
 *  - weather: String ("clear" or "rain")
 *  - temperature: Number
 *  - age: Number (> 0)
 *  - date: String (format: "dd-mm-yyyy")
 *
 * A boolean rule is defined as: "{ <operator>: [<condition>] }"
 * Where:
 * - <operator>: operator name: "@or" or "@and"
 * - <condition>: a value rule or a boolean rule
 *
 * Example:
 * {
 *   "name": "WeatherCode",
 *   "avantage": { "percent": 20 },
 *   "restrictions": [
 *     { "@or": [
 *       { "age": { "@eq": 40 } },
 *       { "age": { "@lt": 30, "@gt": 15 } }
 *     ] },
 *     { "date": { "@lt": "2017-05-02", "@gt": "2018-05-02" },
 *     { "weather": { "@eq": "clear" } },
 *     { "temperature": { "@gt": 15 } }
 *   ]
 * }
 *
 * @param  {Request}   req  Express request
 * @param  {Response}  res  Express response
 * @param  {Function}  next Done callback
 */
function validate(req, res, next) {
  const { body } = req;
  const requiredParameters = ['name', 'avantage'];

  // Check required fields
  for (let param of requiredParameters) {
    if (_.isUndefined(body[param])) {
      throw new ApiError(codes.HTTP.BAD_REQUEST, `missing required field "${param}"`);
    }
  }

  // Check fields type
  if (!_.isString(body.name)) {
    throw new ApiError(codes.HTTP.BAD_REQUEST, 'invalid "name": must be a string');
  } else if (!_.isObject(body.avantage)) {
    throw new ApiError(codes.HTTP.BAD_REQUEST, 'invalid "avantage": must be an object');
  }

  // Check avantage.percent: required and must between 0 and 100
  if (_.isUndefined(body.avantage.percent)
    || !_.isNumber(body.avantage.percent)
    || body.avantage.percent < 0
    || body.avantage.percent > 100) {

    throw new ApiError(codes.HTTP.BAD_REQUEST, 'invalid "avantage.percent": must be between 0 and 100');
  }

  // Check restrictions if provided
  if (!_.isUndefined(body.restrictions)) {
    const error = services.restrictions.validate(body.restrictions);

    if (error) {
      throw new ApiError(codes.HTTP.BAD_REQUEST, `invalid "restriction": ${error}`);
    }
  }

  next();
}

/**
 * handle handles the "/register" request.
 * It inserts the new promo-code in the database
 *
 * NOTE: The request must have been validated before.
 *
 * @param  {Request}   req  Express request
 * @param  {Response}  res  Express response
 * @param  {Function}  next Done callback
 */
function handle(req, res, next) {
  const { name, avantage, restrictions } = req.body;

  return services.promocode.create(name, avantage, restrictions)
    .catch(services.promocode.AlreadyExistsError, () => {
      throw new ApiError(codes.COMMON.ALREADY_EXISTS);
    })
    .then(() => { res.send(); })
    .asCallback(next);
}

module.exports = {
  handle,
  validate
};
