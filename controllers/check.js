'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const Prometheus = require('prom-client');
const moment = require('moment');
const services = require('../services');
const { ApiError, codes } = require('../libs/error');

const invalidPromocodeMetric = new Prometheus.Counter({
  name: 'invalid_promo_code_counter',
  help: 'Invalid promo-code counter',
  labelNames: ['name']
});

const validPromocodeMetric = new Prometheus.Counter({
  name: 'valid_promo_code_counter',
  help: 'valid promo-code counter',
  labelNames: ['name']
});

/**
 * validate is a middleware responsible for validating the incoming request
 *
 * Rules are:
 * - "name" is required and must be a string
 * - "user" is required and must be an object that must contains the
 * following fields:
 *    - "age": required and must be a positive number
 *    - "town": required and must be a non empty string
 *
 * Example:
 * {
 *   "name": "WeatherCode",
 *   "user": {
 *     "age": 25,
 *     "town": "paris"
 *   }
 * }
 *
 * @param  {Request}   req  Express request
 * @param  {Response}  res  Express response
 * @param  {Function}  next Done callback
 */
function validate(req, res, next) {
  const { body }  = req;
  const requiredFields = ['name', 'user'];
  const requiredUserFields = ['age', 'town'];

  // Check required fields
  for (let param of requiredFields) {
    if (_.isUndefined(body[param])) {
      throw new ApiError(codes.HTTP.BAD_REQUEST, `missing required field "${param}"`);
    }
  }

  // Check fields type
  if (!_.isString(body.name)) {
    throw new ApiError(codes.HTTP.BAD_REQUEST, 'invalid "name": must be a string');
  } else if (!_.isObject(body.user)) {
    throw new ApiError(codes.HTTP.BAD_REQUEST, 'invalid "user": must be an object');
  }
  // Check required user fields
  for (let param of requiredUserFields) {
    if (_.isUndefined(body.user[param])) {
      throw new ApiError(codes.HTTP.BAD_REQUEST, `missing required field "user.${param}"`);
    }
  }

  // Check user fields types
  if (!_.isNumber(body.user.age) || body.user.age < 0)  {
    throw new ApiError(codes.HTTP.BAD_REQUEST, 'invalid "user.age": must be a positive number');
  } else if (!_.isString(body.user.town) || body.user.town.length == 0) {
    throw new ApiError(codes.HTTP.BAD_REQUEST, 'invalid "user.town": must be a non empty string');
  }

  next();
}

/**
 * handle handles the "/check" request.
 * It fetches the promocode associated with the given name and check
 * if the given user can use the promocode. A user can use the code
 * only if it satisfy the restrictions defined in the promocode.
 *
 * NOTE: The request must have been validated before.
 *
 * @param  {Request}   req  Express request
 * @param  {Response}  res  Express response
 * @param  {Function}  next Done callback
 */
function handle(req, res, next) {
  const { name, user } = req.body;

  return Promise.all([
    services.weather.find(user.town),
    services.promocode.find(name)
  ])
    .catch(services.promocode.NotFoundError, () => {
      throw new ApiError(codes.HTTP.NOT_FOUND, 'promocode not found');
    })
    .catch(services.weather.NotFoundError, () => {
      throw new ApiError(codes.HTTP.NOT_FOUND, 'town not found');
    })
    .then(([weather, promoCode]) => {
      const { avantage, restrictions } = promoCode;
      const reason = services.restrictions.check({
        age: user.age,
        weather: weather.type,
        temperature: weather.temperature,
        date: moment()
      }, restrictions);

      if (reason) {
        invalidPromocodeMetric.labels(name).inc(1);
        throw new ApiError(codes.HTTP.FORBIDDEN, { reason });
      }

      validPromocodeMetric.labels(name).inc(1);
      res.send({ name, avantage });
    })
    .asCallback(next);
}

module.exports = { handle, validate };
