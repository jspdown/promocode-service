'use strict';

const health = require('./health');
const register = require('./register');
const check = require('./check');

module.exports = { health, register, check };
