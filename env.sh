#!/bin/bash

export NODE_ENV=dev
export API_PORT=7000
export MONGODB_URI=mongodb://localhost:27017/promocode
export REDIS_URL=redis://localhost:6379
export OPEN_WEATHER_MAP_APP_ID=12345
