'use strict';

const express = require('express');
const Prometheus = require('prom-client');
const controllers = require('./controllers');

const router = express.Router();

function prometheusMetrics(req, res) {
  res.set('Content-Type', Prometheus.register.contentType);
  res.end(Prometheus.register.metrics());
}

router
  .get('/', controllers.health)
  .get('/metrics', prometheusMetrics)
  .post('/register',
    controllers.register.validate,
    controllers.register.handle)
  .post('/check',
    controllers.check.validate,
    controllers.check.handle);

module.exports = router;
